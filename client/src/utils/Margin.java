/*
 * @author apmishutkin@edu.hse.ru
 */

package utils;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

public class Margin {
    /**
     * Set margin around component
     */
    static public JComponent setMargin(JComponent component, int top, int left, int bottom, int right) {
        Border border = component.getBorder();
        if (border == null) {
            component.setBorder(new EmptyBorder(top, left, bottom, right));
        } else {
            component.setBorder(new CompoundBorder(new EmptyBorder(top, left, bottom, right), border));
        }
        return component;
    }

    /**
     * Set margin around component
     */
    static public JComponent setMargin(JComponent component, int v, int h) {
        return setMargin(component, v, h, v, h);
    }

    /**
     * Set margin around component
     */
    static public JComponent setMargin(JComponent component, int margin) {
        return setMargin(component, margin, margin);
    }
}
