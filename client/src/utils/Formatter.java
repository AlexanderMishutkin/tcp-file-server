/*
 * @author apmishutkin@edu.hse.ru
 */

package utils;

/**
 * Utility class to create html code from strings
 */
public class Formatter {
    /**
     * @return bold html text from normal string
     */
    static public String bold(String title) {
        return html(String.format("<b>%s</b>", title));
    }

    /**
     * Surrounds text with html tags
     */
    static public String html(String title) {
        return String.format("<html>%s</html>", title);
    }
}
