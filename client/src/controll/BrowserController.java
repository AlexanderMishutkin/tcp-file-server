/*
 * @author apmishutkin@edu.hse.ru
 */

package controll;

import model.FileInfo;
import view.Dialogs;
import view.R;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Some action listeners builders.
 */
public class BrowserController {
    /**
     * @return action listener for footer menu item
     */
    static public ActionListener openFileInExplorer(File path) {
        return (e) -> {
            try {
                Desktop desktop = Desktop.getDesktop();
                desktop.open(path);
            } catch (Exception exception) {
                Dialogs.fileNotOpened(path.getAbsolutePath());
            }
        };
    }

    /**
     * Checks correctness of fields port and url in main window, than calls downloader method connect.
     */
    static public void connect() {
        int port = 80;
        if (!R.port.getText().isBlank()) {
            try {
                port = Integer.parseInt(R.port.getText());
            } catch (Exception ex) {
                Dialogs.badPort(R.port.getText());
                R.port.setText("");
                return;
            }

            if (port <= 0 || port > 65535) {
                Dialogs.badPort(R.port.getText());
                R.port.setText("");
                return;
            }
        }

        if (R.port.getText().isBlank()) {
            R.port.setText("80");
        }

        if (R.url.getText().isBlank()) {
            R.url.setText("127.0.0.1");
        }

        try {
            R.url.setText(URLEncoder.encode(R.url.getText(), java.nio.charset.StandardCharsets.UTF_8.toString()));
        } catch (UnsupportedEncodingException ignored) {
        }

        R.downloader.connect(R.url.getText(), port);
    }

    /**
     * @return ENTER handler for query string
     */
    static public KeyListener enterInQueryString() {
        return new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    connect();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        };
    }

    /**
     * @return handler that allows only numerical input
     */
    static public KeyListener enterInNumberField() {
        return new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                JTextField field = (JTextField) e.getSource();
                if (field.getText().matches("[0-9\\-]*[^0-9\\-]+[0-9\\-]*|[0-9\\-]+-[0-9\\-]*")) {
                    field.setText(field.getText()
                            .replaceAll("[^0-9\\-]|(?<=.)-", ""));
                }
            }
        };
    }

    /**
     * @return Action listener for download button.
     */
    public static ActionListener downloadPress(FileInfo fileInfo) {
        return (e) -> R.downloader.queueFile(fileInfo.clone());
    }

    /**
     * @return Action listener for main window's cross.
     */
    public static WindowListener close() {
        return new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                if (R.downloader.close()) {
                    System.exit(0);
                }
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        };
    }
}
