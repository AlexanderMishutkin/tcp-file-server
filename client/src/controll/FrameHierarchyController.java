/*
 * @author      apmishutkin@edu.hse.ru
 */

package controll;

import javax.swing.*;

/**
 * Gives ability to easily get parent while dialog creation
 */
public class FrameHierarchyController {
    private static JFrame mainFrame = new JFrame(); // Stub! Will be set to the real main frame while working...

    public static void setMainFrame(JFrame mainFrame) {
        FrameHierarchyController.mainFrame = mainFrame;
    }

    /**
     * Returns main frame of program. In current case - good variant for dialog parent
     *
     * @return main frame of program
     */
    public static JFrame getMainFrame() {
        return mainFrame;
    }
}
