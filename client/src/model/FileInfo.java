/*
 * @author apmishutkin@edu.hse.ru
 */

package model;

import java.io.Serializable;

/**
 * Class to serialize and send via socket to the client to choice which file to download.
 * <p>
 * Despite so much "to"s it is the simplest class)
 */
public class FileInfo implements Serializable, Cloneable {
    private String name;
    private long size;
    private String pathToSave;

    /**
     * @return simple file name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name file name (just <code>File.getName()</code>)
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return size in bytes.
     */
    public long getSize() {
        return size;
    }

    /**
     * @param size in bytes.
     */
    public void setSize(long size) {
        this.size = size;
    }

    /**
     * @return file size in <code>.2f</code> format with units name.
     */
    public String getSizeFormatted() {
        if (getSize() == 0) {
            return "0";
        }
        StringBuilder answer = new StringBuilder();
        if (getSize() / 1024L / 1024L / 1024L > 0) {
            answer.append(String.format("%.2f ГБ", getSize() / 1024D / 1024D / 1024D));
            return answer.toString();
        }
        if (getSize() / 1024L / 1024L > 0) {
            answer.append(String.format("%.2f МБ", getSize() / 1024D / 1024D % 1024L));
            return answer.toString();
        }
        if (getSize() / 1024L > 0) {
            answer.append(String.format("%.2f КБ", getSize() / 1024D % 1024L));
            return answer.toString();
        }
        answer.append(String.format("%d Б", getSize()));
        return answer.toString();
    }

    /**
     * (Is being used and set in client only)
     *
     * @return path to save.
     */
    public String getPathToSave() {
        return pathToSave;
    }

    /**
     * (Is being used and set in client only)
     *
     * @param pathToSave path to save.
     */
    public void setPathToSave(String pathToSave) {
        this.pathToSave = pathToSave;
    }

    /**
     * @return full deep copy.
     */
    @Override
    public FileInfo clone() {
        try {
            return (FileInfo) super.clone();
        } catch (CloneNotSupportedException e) {
            return new FileInfo();
        }
    }
}
