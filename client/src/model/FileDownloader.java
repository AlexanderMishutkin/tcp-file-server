/*
 * @author apmishutkin@edu.hse.ru
 */

package model;

import view.Dialogs;
import view.ProgressBar;
import view.R;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.nio.file.Path;
import java.util.*;

/**
 * Complicated class to download files from server.
 * <p>
 * Luckily, I do not need to document non-public methods))
 */
public class FileDownloader {
    final List<FileInfo> queue = new LinkedList<>();
    final Object queueChanged = new Object();
    final List<File> history = new ArrayList<>();
    volatile Socket socket;
    ObjectInputStream in;
    ObjectOutputStream out;
    Thread downloadLoop = null;

    /**
     * Creates unconnected downloader.
     */
    public FileDownloader() {
        // start();
    }

    /**
     * Asks user for path to save, than add file to queue.
     * It will be downloaded when previous requests would be finished.
     */
    public synchronized void queueFile(FileInfo fileInfo) {
        synchronized (queue) {
            if (socket != null && !socket.isClosed() && socket.isConnected() && setDownloadPath(fileInfo)) {
                if (queue.stream()
                        .noneMatch((fileInfo1 -> fileInfo1.getPathToSave().equals(fileInfo.getPathToSave())))) {
                    queue.add(fileInfo);
                    R.footer.RefreshNumbers(history.size(), queue.size() + history.size());
                } else {
                    Dialogs.pathIsUsed(fileInfo.getPathToSave());
                }
            }
        }
        synchronized (queueChanged) {
            queueChanged.notify();
        }
    }

    /**
     * Downloads files till queue is not empty.
     */
    void runDownloadLoop() {
        try {
            in = new ObjectInputStream(socket.getInputStream());
            out = new ObjectOutputStream(socket.getOutputStream());

            @SuppressWarnings("unchecked") List<FileInfo> files = (ArrayList<FileInfo>) in.readObject();
            R.chooser.updateFileList(files);

            while (socket.isConnected() && !socket.isClosed()) {
                FileInfo fileInfo = null;
                synchronized (queue) {
                    if (!queue.isEmpty()) {
                        fileInfo = queue.get(0);
                    }
                }
                if (fileInfo != null) {
                    download(fileInfo);
                } else {
                    try {
                        synchronized (queueChanged) {
                            queueChanged.wait(5);
                        }
                    } catch (InterruptedException ignored) {
                    }
                }
            }
        } catch (Exception exception) {
            R.chooser.updateFileList(List.of());
            R.port.setText("");
            R.url.setText("");

            if (R.progress != null) {
                new Thread(R.progress::close).start();
                R.progress = null;
            }
        }
    }

    /**
     * Connects to the server. Terminates previous connections.
     *
     * @param path correct server url
     * @param port correct server port number
     */
    synchronized public void connect(String path, int port) {
        if (queue.isEmpty() || Dialogs.clearQueue() == Dialogs.YES) {
            try {
                if (socket != null && socket.isConnected() && !socket.isClosed()) {
                    socket.close();
                }

                if (downloadLoop != null) {
                    try {
                        downloadLoop.join();
                    } catch (InterruptedException ignored) {
                    }
                }

                synchronized (queue) {
                    deleteUnfinished();
                    R.footer.RefreshNumbers(0, 0);
                }

                socket = new Socket(path, port);


                downloadLoop = new Thread(FileDownloader.this::runDownloadLoop);
                downloadLoop.start();
            } catch (IOException exception) {
                Dialogs.noConnection(path, port);
                R.chooser.updateFileList(List.of());
            }
        }
    }

    /**
     * Let user choose where to save file.
     */
    boolean setDownloadPath(FileInfo fileInfo) {
        JFileChooser jfc = new JFileChooser();

        jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        jfc.setFileFilter(new javax.swing.filechooser.FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.isDirectory();
            }

            @Override
            public String getDescription() {
                return "Directories";
            }
        });

        if (jfc.showSaveDialog(new JPanel()) == JFileChooser.APPROVE_OPTION) {
            File toSave = jfc.getSelectedFile().toPath().resolve(Path.of(fileInfo.getName())).toFile();
            if (toSave.exists()) {
                if (Dialogs.rewriteFile(toSave.getName()) == Dialogs.NO) {
                    return false;
                }
            }

            fileInfo.setPathToSave(toSave.getAbsolutePath());
            return true;
        }
        return false;
    }

    /**
     * Downloads file.
     */
    void download(FileInfo fileInfo) throws IOException, ClassNotFoundException {
        File file = new File(fileInfo.getPathToSave());
        R.progress = new ProgressBar(fileInfo);
        try (FileOutputStream fOut = new FileOutputStream(file)) {
            out.writeObject(fileInfo);
            int size;
            long total = 0;
            while ((size = (int) in.readObject()) > 0) {
                fOut.write((byte[]) in.readObject(), 0, size);
                total += size;
                R.progress.updateTotal(total);
            }
        }
        synchronized (queue) {
            history.add(0, file);
            queue.remove(0);
            R.footer.addFile(file, history.size(), history.size() + queue.size());
            R.progress.close();
            R.progress = null;
        }
    }

    /**
     * Terminate downloading.
     *
     * @return false, if user stopped closing and resume downloading.
     */
    public boolean close() {
        synchronized (queue) {
            if (!queue.isEmpty()) {
                if (Dialogs.clearQueue() == Dialogs.NO) {
                    return false;
                }
            }
        }

        if (socket != null && socket.isConnected() && !socket.isClosed()) {
            try {
                socket.close();
            } catch (IOException ignored) {
            }
        }

        if (downloadLoop != null) {
            try {
                downloadLoop.join();
            } catch (InterruptedException ignored) {
            }
        }

        synchronized (queue) {
            deleteUnfinished();
        }

        return true;
    }

    /**
     * Removes corrupted files.
     */
    private void deleteUnfinished() {
        if (!queue.isEmpty()) {
            File file;
            if ((file = new File(queue.get(0).getPathToSave())).exists()) {
                try {
                    boolean delete = file.delete();
                    if (!delete) {
                        System.out.println("Осторожно! Недозагруженный файл остался на диске...");
                    }
                } catch (Exception ignored) {
                }
            }
        }
        queue.clear();
    }
}
