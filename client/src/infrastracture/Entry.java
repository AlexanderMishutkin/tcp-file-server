/*
 * @author apmishutkin@edu.hse.ru
 */

package infrastracture;

import model.FileDownloader;
import view.Browser;
import view.R;

/**
 * Client's entry point.
 */
public class Entry {
    /**
     * Starts client.
     */
    public static void main(String[] args) {
        R.downloader = new FileDownloader();
        R.main = new Browser();
    }
}
