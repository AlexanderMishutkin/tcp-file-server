/*
 * @author apmishutkin@edu.hse.ru
 */

package view;

import javax.swing.*;

import static controll.FrameHierarchyController.getMainFrame;

/**
 * Show some useful dialogs.
 */
public class Dialogs {

    static public final int YES = 0;
    static public final int NO = 1;

    /**
     * Should we terminate downloading and clear queue?
     */
    static public int clearQueue() {
        Object[] options = {"Прервать скачивание", "Отмена"};
        return JOptionPane.showOptionDialog(R.main,
                "Сейчас еще загружаются файлы со старого сервера.\nПрервать загрузку и отчистить очередь?",
                "Прервать скачивание?",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[1]);
    }

    /**
     * Can not connect to server!
     */
    static public void noConnection(String url, int port) {
        JOptionPane.showMessageDialog(getMainFrame(),
                String.format("Сервер по адресу %s:%d не предоставил соединения!", url, port),
                "Ошибка подключения!",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * User entered not real port.
     */
    static public void badPort(String port) {
        JOptionPane.showMessageDialog(getMainFrame(),
                String.format("%s не является номером порта - то есть числом в диапозоне 0-65535", port),
                "Ошибка подключения!",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Rewrite existing file?
     */
    public static int rewriteFile(String selectedFile) {
        Object[] options = {"Заменить", "Отмена"};
        return JOptionPane.showOptionDialog(R.main,
                String.format("Файл с именем %s уже существует. Заменить его?", selectedFile),
                "Выбран существующий файл!",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[1]);
    }

    /**
     * Can not rewrite file, that is even not downloaded yet!
     */
    public static void pathIsUsed(String pathToSave) {
        JOptionPane.showMessageDialog(getMainFrame(),
                String.format("Выбранный путь (\"%s\")\nуже используется для сохранения другого файла!", pathToSave),
                "Ошибка скачивания!",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Can not open file in explorer...
     */
    public static void fileNotOpened(String path) {
        JOptionPane.showMessageDialog(getMainFrame(),
                String.format("Файл (\"%s\")\nне получается открыть средствами вашей ОС.\nпопробуйте сделать это сами)",
                        path),
                "Нет средств открытия файла по умолчанию...",
                JOptionPane.INFORMATION_MESSAGE);
    }
}
