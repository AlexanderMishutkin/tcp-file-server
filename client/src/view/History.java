/*
 * @author apmishutkin@edu.hse.ru
 */

package view;

import controll.BrowserController;
import utils.Formatter;

import javax.swing.*;
import java.awt.*;
import java.io.File;

/**
 * Represents downloaded files.
 */
public class History extends JMenuBar {
    JMenu downloadedText;

    /**
     * Creates menu bar with downloaded files' names.
     */
    public History() {
        setLayout(new FlowLayout(FlowLayout.LEFT));
        downloadedText = new JMenu(Formatter.bold("Загружено:"));
        downloadedText.setEnabled(false);
        add(downloadedText);
    }

    /**
     * Change counters.
     */
    public void RefreshNumbers(int downloadedNumber, int totalNumber) {
        if (totalNumber == 0) {
            downloadedText.setText(Formatter.bold("Нет загруженных файлов..."));
            return;
        }
        downloadedText.setEnabled(true);
        if (downloadedNumber == totalNumber) {
            downloadedText.setText(Formatter.bold(String.format("Загружено [%d]:", totalNumber)));
        } else {
            downloadedText
                    .setText(Formatter.bold(String.format("Загружаются [%d/%d]:", downloadedNumber, totalNumber)));
        }
    }

    static JMenu downloaded(File path) {
        JMenu downloaded = new JMenu(path.getName());
        JMenuItem open = new JMenuItem("Открыть файл");
        JMenuItem show = new JMenuItem("Показать в проводнике");
        open.addActionListener(BrowserController.openFileInExplorer(path));
        show.addActionListener(BrowserController.openFileInExplorer(path.getParentFile()));
        downloaded.add(open);
        downloaded.add(show);
        return downloaded;
    }

    /**
     * Change counters and add one more downloaded file to history.
     */
    public synchronized void addFile(File path, int downloadedNumber, int totalNumber) {
        RefreshNumbers(downloadedNumber, totalNumber);

        JMenu downloaded = downloaded(path);
        JMenu downloaded1 = downloaded(path);

        if (getComponents().length <= 1) {
            JMenu info = new JMenu("⟵ Нажмите на файл чтобы его открыть");
            info.setEnabled(false);
            add(info);
        }
        revalidate();
        add(downloaded1, 1);
        downloadedText.add(downloaded);
        invalidate();
        getRootPane().repaint();
    }
}
