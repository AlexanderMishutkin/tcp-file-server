/*
 * @author apmishutkin@edu.hse.ru
 */

package view;

import controll.BrowserController;
import utils.Formatter;
import utils.Margin;

import javax.swing.*;
import java.awt.*;

/**
 * Browsers query string (literally)
 */
public class QueryString extends JPanel {
    /**
     * Creates string and adds all the components.
     */
    public QueryString() {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        // Drawing elements

        R.update = new JButton(Formatter.bold("⭯"));
        JLabel prefix = new JLabel(Formatter.bold("tcp://"));
        R.url = new JTextField();
        JLabel dots = new JLabel(Formatter.bold(":"));
        R.port = new JTextField();
        R.connect = new JButton(Formatter.bold("Искать!"));

        R.update.setMaximumSize(new Dimension(5, R.update.getPreferredSize().height * 2));
        prefix.setMaximumSize(new Dimension(10, R.update.getPreferredSize().height * 2));
        dots.setMaximumSize(new Dimension(10, R.update.getPreferredSize().height * 2));
        R.connect.setMaximumSize(new Dimension(30, R.update.getPreferredSize().height * 2));

        add(Margin.setMargin(R.update, 2), BorderLayout.CENTER);
        add(Margin.setMargin(prefix, 5), BorderLayout.CENTER);
        add(Margin.setMargin(R.url, 3), BorderLayout.CENTER);
        add(Margin.setMargin(dots, 5), BorderLayout.CENTER);
        add(Margin.setMargin(R.port, 3), BorderLayout.CENTER);
        add(Margin.setMargin(R.connect, 5), BorderLayout.CENTER);

        R.port.addKeyListener(BrowserController.enterInQueryString());
        R.port.addKeyListener(BrowserController.enterInNumberField());
        R.url.addKeyListener(BrowserController.enterInQueryString());
        R.update.addActionListener((e) -> BrowserController.connect());
        R.connect.addActionListener((e) -> BrowserController.connect());
    }
}
