/*
 * @author apmishutkin@edu.hse.ru
 */

package view;

import controll.BrowserController;
import controll.FrameHierarchyController;
import utils.Localization;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Main app's window.
 */
public class Browser extends JFrame {
    /**
     * For testing needs...
     */
    static boolean VISIBLE = true;

    /**
     * Creates and shows main window.
     */
    public Browser() {
        super("Быстрооблако ©");
        Localization.localize();

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); // Default look and feel is foobar :(
        } catch (Exception ignored) {

        }
        AddElements();

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setMinimumSize(new Dimension(600, 400));

        try {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int screenHeight = screenSize.height;
            int screenWidth = screenSize.width;

            setLocation(screenWidth / 2 - 300, screenHeight / 2 - 200);
        } catch (Exception ignored) {

        }

        pack();
        setVisible(VISIBLE);

        FrameHierarchyController.setMainFrame(this); // Dialogs can be created with this as parent
        addWindowListener(BrowserController.close());
    }

    /**
     * Creates components.
     */
    private void AddElements() {
        JPanel contentPane = (JPanel) getContentPane();
        contentPane.add(new JScrollPane(), BorderLayout.CENTER);

        R.header = new QueryString();
        contentPane.add(R.header, BorderLayout.NORTH);

        R.footer = new History();

//        JScrollPane jScrollPane = new JScrollPane(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,
//                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
//        jScrollPane.add(R.footer);
//        jScrollPane.revalidate();
//        jScrollPane.repaint();
//        jScrollPane.setMinimumSize(new Dimension(jScrollPane.getMinimumSize().width, 50));
        contentPane.add(R.footer, BorderLayout.SOUTH);

        R.chooser = new ClientFileChooser();
        contentPane.add(new JScrollPane(R.chooser));
        R.chooser.updateFileList(List.of());
    }
}
