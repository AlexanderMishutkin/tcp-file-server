/*
 * @author apmishutkin@edu.hse.ru
 */

package view;

import model.FileDownloader;

import javax.swing.*;

/**
 * One of the main classes in app - stores static links to app's components.
 * <p>
 * Full analog of Android "R" class. Global indexation universe.
 */
public class R {
    // Components
    static volatile public FileDownloader downloader;
    public volatile static History footer;
    public volatile static QueryString header;
    public volatile static ClientFileChooser chooser;

    // Text fields & other inputs
    static volatile public JTextField url;
    static volatile public JTextField port;

    // Buttons & etc
    public volatile static JButton connect;
    public volatile static JButton update;

    // Frames
    static volatile public Browser main;
    static volatile public ProgressBar progress;
}
