/*
 * @author apmishutkin@edu.hse.ru
 */

package view;

import model.FileInfo;
import utils.Margin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;

import static view.Browser.VISIBLE;

/**
 * Represents download success.
 */
public class ProgressBar extends JFrame {
    private final String msgText = "<html><b>Загружаем файл \"%s\"</b>:<br/>" +
            "[%s из %s]<br/>" +
            "<br/>" +
            "<i>*Вы можете закрыть это окно - загрузка продолжится фоном.<br/>" +
            "**Для прерывания загрузки обновите соединение (⭯) на главном экране.</i>";
    JLabel msg;
    JProgressBar bar;
    FileInfo fileInfo;

    /**
     * Initialize progress bar for current file-downloading process.
     */
    public ProgressBar(FileInfo fileInfo) {
        super("Быстрооблако ©");
        msg = new JLabel(
                String.format(msgText, fileInfo.getName(), "0", fileInfo.getSizeFormatted()));
        this.fileInfo = fileInfo;
        bar = new JProgressBar();

        msg.setPreferredSize(new Dimension(msg.getMaximumSize().width, 100));
        bar.setPreferredSize(new Dimension(bar.getMaximumSize().width, 15));
        JPanel pane = new JPanel(new GridLayout(1, 1));
        pane.add(bar);
        pane.setPreferredSize(new Dimension(350, 15));

        Container panel = getContentPane();
        panel.setLayout(new GridLayout(2, 1));
        panel.add(Margin.setMargin(msg, 25, 10));
        panel.add(Margin.setMargin(pane, 20, 10));

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(400, 200));
        setResizable(false);

        try {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int screenHeight = screenSize.height;
            int screenWidth = screenSize.width;

            setLocation(screenWidth / 2 - 200, screenHeight / 2 - 100);
        } catch (Exception ignored) {

        }

        pack();
        setVisible(VISIBLE);
    }

    /**
     * Change progress.
     *
     * @param downloaded already downloaded bytes.
     */
    public void updateTotal(long downloaded) {
        FileInfo cur = new FileInfo();
        cur.setSize(downloaded);
        msg.setText(
                String.format(msgText, fileInfo.getName(), cur.getSizeFormatted(), fileInfo.getSizeFormatted()));
        if (fileInfo.getSize() >= 100) {
            bar.setValue((int) (downloaded / (fileInfo.getSize() / 100L)));
        }
    }

    /**
     * Close this window from code.
     */
    public void close() {
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }
}
