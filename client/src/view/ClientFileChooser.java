/*
 * @author apmishutkin@edu.hse.ru
 */

package view;

import controll.BrowserController;
import model.FileInfo;
import utils.Formatter;
import utils.Margin;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Shows client files on server.
 */
public class ClientFileChooser extends JPanel {
    /**
     * Shows no files by default.
     */
    public ClientFileChooser() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    /**
     * Change files on screen to one offered by current server. Gives ability to download them.
     *
     * @param files files to represent.
     */
    public void updateFileList(List<FileInfo> files) {
        removeAll();
        if (files.isEmpty()) {
            add(Margin.setMargin(new JLabel(
                    "<html>Файлов для скачивания не найдено...<br/>" +
                            "Воспользуйтесь поисковой строкой чтобы подключиться к облаку.</html>"), 10));
            invalidate();
            getRootPane().repaint();
            return;
        }
        JPanel cap = new JPanel();
        cap.setLayout(new GridLayout(1, 3));
        cap.setMaximumSize(new Dimension(cap.getMaximumSize().width, 30));
        cap.add(new JLabel(Formatter.bold("Имя")));
        cap.add(new JLabel(Formatter.bold("Размер")));
        cap.add(new JLabel(Formatter.bold("Действие")));
        for (Component component : cap.getComponents()) {
            ((JLabel) component).setHorizontalAlignment(SwingConstants.CENTER);
            Margin.setMargin(((JLabel) component), 5);
        }
        add(cap);

        for (FileInfo file : files) {
            JPanel row = new JPanel();
            row.setLayout(new GridLayout(1, 3));
            row.setMaximumSize(new Dimension(row.getMaximumSize().width, 30));

            String nameText = file.getName();
            if (nameText.length() > 25) {
                nameText = nameText.substring(0, 12) + "..." + nameText.substring(nameText.length() - 12);
            }

            JLabel name = new JLabel(Formatter.bold(nameText));
            name.setHorizontalTextPosition(SwingConstants.LEFT);
            row.add(name);

            JLabel size = new JLabel(Formatter.html(file.getSizeFormatted()));
            size.setHorizontalTextPosition(SwingConstants.RIGHT);
            row.add(Margin.setMargin(size, 0, 10));

            JButton btn = new JButton("Скачать \uD83E\uDC47");
            btn.setMaximumSize(new Dimension(20, btn.getMaximumSize().height));
            row.add(btn);
            btn.addActionListener(BrowserController.downloadPress(file));
            add(Margin.setMargin(row, 5));
        }
        invalidate();
        getRootPane().repaint();
    }
}
