package mockup;

import model.FileInfo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class StupidServer extends Thread {
    public static final String HOST = "0.0.0.0";
    public static final String FILE_NAME = "A.txt";
    public static final int PORT = 8080;
    public static final String FILE_CONTENT = "EXAMPLE";
    public final AtomicBoolean isRunning = new AtomicBoolean(false);
    volatile ServerSocket socket;
    public static final byte[] FILE = FILE_CONTENT.getBytes();

    @Override
    public void run() {
        try {
            socket = new ServerSocket(PORT, 50, InetAddress.getByName(HOST));
            isRunning.set(true);
            Socket client = socket.accept();
            new Thread(()->{
                try {
                    ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
                    ObjectInputStream in = new ObjectInputStream(client.getInputStream());

                    ArrayList<FileInfo> variants = new ArrayList<>();
                    variants.add(new FileInfo());
                    variants.get(0).setName(FILE_NAME);
                    variants.get(0).setSize(FILE.length);
                    out.writeObject(variants);
                    out.flush();
                    out.reset();

                    FileInfo fileInfo = (FileInfo) in.readObject();
                    if (fileInfo.getName().equals(FILE_NAME)) {
                        out.writeObject(FILE.length);
                        out.writeObject(FILE);
                        out.reset();
                        out.writeObject(-1);
                        out.flush();
                        out.reset();
                    }
                    client.close();
                } catch (Exception ignored) {
                }
            }).start();
        } catch (Exception ignored) {
        }
    }

    public synchronized boolean startServer() {
        start();
        int cnt = 0;
        while (!isRunning.get() && cnt++ < 500) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException ignored) {
            }
        }
        return isRunning.get();
    }

    public synchronized void finishServer() {
        isRunning.set(false);
        try {
            socket.close();
        } catch (IOException ignored) {
        }

        try {
            join();
        } catch (InterruptedException ignored) {
        }
    }
}
