package view;

import infrastracture.Entry;
import mockup.StupidServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.util.Arrays;

import static mockup.StupidServer.FILE_NAME;
import static org.junit.jupiter.api.Assertions.*;

public class BrowserTest {
    StupidServer server;

    @BeforeEach
    public void setUp() {
        server = new StupidServer();
        assertTrue(server.startServer());
        Browser.VISIBLE = false;
    }

    @AfterEach
    public void tearDown() {
        assertTrue(server.isRunning.get());
        server.finishServer();
    }

    @Test
    public void fullFunctionalTest() {
        Entry.main(new String[]{});
        assertNotNull(R.main);

        R.url.setText(StupidServer.HOST);
        R.port.setText(String.valueOf(StupidServer.PORT));
        R.connect.doClick();

        int cnt = 0;
        while (R.chooser.getComponents().length < 2) {
            if (cnt++ > 500) {
                fail("Данные с сервера не грузятся уже пол секунды. А вы на одном с ним хосте!!!");
            }
            try {
                //noinspection BusyWait
                Thread.sleep(1);
            } catch (InterruptedException ignored) {
            }
        }
        assertTrue(Arrays.stream(R.chooser.getComponents()).anyMatch(c ->
                Arrays.stream(((JPanel) c).getComponents())
                        .anyMatch((l) -> {
                            if (l instanceof JButton) {
                                return ((JButton) l).getText().matches("^.*" + FILE_NAME + ".*$");
                            }
                            if (l instanceof JLabel) {
                                return ((JLabel) l).getText().matches("^.*" + FILE_NAME + ".*$");
                            }
                            return false;
                        })));
    }
}