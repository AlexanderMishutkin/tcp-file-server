package model;

import mockup.StupidServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import view.BrowserTest;
import view.R;

import javax.swing.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class FileDownloaderTest {
    BrowserTest browserTest = new BrowserTest();


    @BeforeEach
    void setUp() {
        browserTest.setUp();
        browserTest.fullFunctionalTest();
    }

    @AfterEach
    void tearDown() {
        browserTest.tearDown();
    }

    @Test
    void download() {
        FileInfo request = new FileInfo();
        request.setSize(StupidServer.FILE.length);
        request.setName(StupidServer.FILE_NAME);
        request.setPathToSave(StupidServer.FILE_NAME);

        synchronized (R.downloader.queue) {
            R.downloader.queue.add(request);
        }
        synchronized (R.downloader.queueChanged) {
            R.downloader.queueChanged.notify();
        }

        int cnt = 0;
        while (R.footer.getComponents().length < 2) {
            if (cnt++ > 500) {
                fail("Данные с сервера не грузятся уже пол секунды. А вы на одном с ним хосте!!!");
            }
            try {
                //noinspection BusyWait
                Thread.sleep(1);
            } catch (InterruptedException ignored) {
            }
        }
        assertTrue(((JMenu) R.footer.getComponent(1))
                .getText().matches("^.*" + StupidServer.FILE_NAME + ".*$"));
        try {
            assertEquals(Files.readString(Path.of(StupidServer.FILE_NAME)), StupidServer.FILE_CONTENT);
        } catch (IOException exception) {
            fail("Приложением записался такой файл, что его потом не прочесть)))");
        }
        boolean delete = new File(StupidServer.FILE_NAME).delete();
        R.downloader.close();
    }
}