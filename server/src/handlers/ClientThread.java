/*
 * @author apmishutkin@edu.hse.ru
 */

package handlers;

import model.FileInfo;
import server.Server;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;

/**
 * Threads that works with current client socket.
 */
public class ClientThread extends Thread {
    /**
     * In bytes :)
     */
    final public static long MB = 1024 * 1024;
    /**
     * Max size of data being sent to client at one solid socket.write() call.
     */
    final public static int CHUNK_SIZE = (int) (32 * MB);
    final private Socket client;

    /**
     * @param client to work with.
     */
    public ClientThread(Socket client) {
        this.client = client;
    }

    /**
     * Listens to client's request until client broke connection, send wrong request or server is shut downed in Sever.
     */
    @Override
    public void run() {
        try {
            System.out.printf("[%s]\t[pid: %d]\t\"%s:%d\" запросил список файлов.%n", new Date(),
                    Thread.currentThread().getId(),
                    client.getInetAddress(), client.getPort());

            ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(client.getInputStream());

            // First, sending list of files
            ArrayList<FileInfo> variants = new ArrayList<>();
            File dir = Server.toShare.toFile();
            File[] listFiles = dir.listFiles();
            if (!dir.isDirectory() || listFiles == null) {
                System.err.printf("Сервер не может передать список файлов: \"%s\" - не папка!%n",
                        dir.getCanonicalPath());
                Server.off();
                return;
            }

            for (File file : listFiles) {
                if (file.isFile()) {
                    FileInfo info = new FileInfo();
                    info.setName(file.getName());
                    info.setSize(Files.size(file.toPath()));
                    variants.add(info);
                }
            }
            out.writeObject(variants);
            out.flush();

            while (!client.isClosed() && client.isConnected() && client.isBound() && Server.serverOn.get()) {
                // Secondly, receive client choice
                FileInfo fileInfo = (FileInfo) in.readObject();
                File toSend = null;
                for (File file : listFiles) {
                    if (fileInfo.getName().equals(file.getName())) {
                        toSend = file;
                    }
                }
                if (toSend == null) {
                    System.out.printf("[%s]\t[pid: %d]\tКлиент запросил файл, который не был предложен \"%s\"!%n",
                            new Date(),
                            Thread.currentThread().getId(),
                            fileInfo.getName());
                    client.close();
                    System.out.printf("[%s]\t[pid: %d]\tОтключаю данного клиента.%n",
                            new Date(),
                            Thread.currentThread().getId());
                    return;
                }
                final long fullSize = Files.size(toSend.toPath());
                long sent = 0;

                // Than sending client choice by chunks
                // Actually, sending (Integer) chunk size or EOF (-1) and chunk (byte[]) in each iteration
                try (FileInputStream fileReader = new FileInputStream(toSend)) {
                    byte[] buf = new byte[CHUNK_SIZE];
                    int bufSize;
                    while ((bufSize = fileReader.read(buf)) != -1 && Server.serverOn.get()) {
                        out.writeObject(bufSize);
                        out.writeObject(buf);
                        System.out.printf("[%s]\t[pid: %d]\tШлю файл \"%s\" [%d\tиз\t%d МБ]%n",
                                new Date(),
                                Thread.currentThread().getId(),
                                fileInfo.getName(),
                                (sent += CHUNK_SIZE) / MB,
                                fullSize / MB);
                        out.reset();
                    }
                    out.writeObject(-1);
                    System.out.printf("[%s]\t[pid: %d]\tФайл \"%s\" успешно передан!%n",
                            new Date(),
                            Thread.currentThread().getId(),
                            fileInfo.getName());
                }
                // Ready to next order from client
            }
        } catch (Exception e) {
            System.out.printf("[%s]\t[pid: %d]\tОтключаю данного клиента.%n",
                    new Date(),
                    Thread.currentThread().getId());
        }
    }
}
