/*
 * @author apmishutkin@edu.hse.ru
 */

package server;

import console.Scanner;
import handlers.ClientThread;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Entry point of server, work with console arguments and starts server.
 */
public class Server {
    volatile static ServerSocket serverSocket = null;
    /**
     * Can server accept client? True only if socket is correctly initialized.
     */
    public volatile static AtomicBoolean serverOn = new AtomicBoolean(false);
    /**
     * Directory, in which server is working "." by default.
     */
    public volatile static Path toShare;

    /**
     * Shutdown server.
     */
    public static void off() {
        serverOn.set(false);
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (IOException ignored) {

        }
    }

    /**
     * Output some help message.
     */
    public static void help() {
        System.out.println("Аргументы командной строки:");
        System.out.println("(-h) --help - запросить подсказку.");
        System.out.println("(-D) --dir=<путь> - путь, по которому лежат файлы для раздачи (\"./\" по умолчанию).");
        System.out.println("(-H) --host=<IPv4 адрес> - адрес, на котором надо поднять сервер (0.0.0.0 по умолчанию).");
        System.out.println(
                "(-P) --port=<целое число от 0 до 65535> - порт, на котором надо поднять сервер (80 по умолчанию).");
        System.out.println("Команды сервера:");
        System.out.println(">> exit - выключение сервера.");
        System.out.println("Сервер выводит информацию о клиентах ввиде лога. Для работы используются TCP/IP сокеты.");
    }

    /**
     * Entry point of server, work with console arguments and starts server.
     *
     * @param args see params description by calling server with <code>--help</code>.
     */
    public static void main(String[] args) {
        // Here goes 50+ lines of stupid work with console args:
        System.out.println("Вас приветсвует сервер системы \"Быстрооблако ©\"!");
        System.out.println("------------------------------------------------");
        if (Arrays.stream(args).anyMatch(s -> s.equals("--help") || s.equals("-h"))) {
            help();
            return;
        }
        String badFlag;
        if ((badFlag = Arrays.stream(args).filter(s ->
                !s.startsWith("-h") && !s.startsWith("--help") &&
                        !s.startsWith("-H=") && !s.startsWith("--host=") &&
                        !s.startsWith("-D=") && !s.startsWith("--directory=") &&
                        !s.startsWith("-P=") && !s.startsWith("--port=")).findAny().orElse(null)) != null) {
            System.err.printf("Неизвестный флаг: \"%s\".%n", badFlag);
            help();
            return;
        }

        String path =
                Arrays.stream(args).filter(s -> s.startsWith("--directory=") | s.startsWith("-D=")).findAny().orElse(
                        "-D=.").replaceAll("--directory=|-D=", "");
        if (!new File(path).exists() || !new File(path).isDirectory()) {
            System.err.printf("Путь \"%s\" не является директроией%n" +
                    "Используйте флаг \"--help\" для получения справки.%n", path);
            return;
        }
        toShare = Path.of(path);

        String hostName =
                Arrays.stream(args).filter(s -> s.startsWith("--host=") | s.startsWith("-H=")).findAny().orElse(
                        "-H=0.0.0.0").replaceAll("--host=|-H=", "");
        InetAddress host;
        try {
            host = InetAddress.getByName(hostName);
        } catch (Exception e) {
            System.err.printf("Хост %s не является валидным адресом%n" +
                    "Используйте флаг \"--help\" для получения справки.%n", hostName);
            return;
        }

        int port;
        try {
            port = Integer.parseInt(
                    Arrays.stream(args).filter(s -> s.startsWith("--port=") | s.startsWith("-P=")).findAny().orElse(
                            "-P=80").replaceAll("--port=|-P=", ""));
            if (port < 0 || port > 65535) {
                System.err.printf("Введен плохой номер порта - не входит в диапазон портов %d%n" +
                        "Используйте флаг \"--help\" для получения справки.%n", port);
                return;
            }
        } catch (Exception e) {
            System.err.printf("Введен плохой номер порта%n" +
                    "Используйте флаг \"--help\" для получения справки.%n");
            return;
        }

        // And here we are starting our server:
        try {
            serverSocket = new ServerSocket(port, 50, host);
        } catch (Exception exception) {
            System.err.printf("Сервер с адресом \"%s:%d\" не был запущен!%n" +
                    "Причиной может быть занятость порта, отсутсвия доступа к данному хосту или иные проблемы.%n" +
                    "Используйте флаг \"--help\" для получения справки.%n", hostName, port);
            return;
        }

        serverOn.set(true);
        System.out.printf("Сервер с адресом \"%s:%d\" запущен над дирикторией \"%s\"!%n" +
                "Введите \"exit\" для его выключения.%n", hostName, port, toShare);
        new Scanner().start();

        try {
            while (serverOn.get()) {
                Socket client = serverSocket.accept();
                new ClientThread(client).start();
            }
        } catch (Exception ignored) {
        } finally {
            System.out.println("Сервер выключен успешно!");
            System.out.println("Спасибо что использовали \"Быстрооблако ©\"!");
        }
    }
}
