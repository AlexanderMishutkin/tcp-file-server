/*
 * @author apmishutkin@edu.hse.ru
 */

package console;

import server.Server;

/**
 * Simple thread that waits for "exit" to be input before shutting server down.
 */
public class Scanner extends Thread {
    /**
     * Waits for "exit" to be input before shutting server down.
     */
    @Override
    public void run() {
        java.util.Scanner scanner = new java.util.Scanner(System.in);
        while (!scanner.nextLine().equalsIgnoreCase("exit")) {
            System.out.println("Введите \"exit\" to finish server...");
        }
        Server.off();
    }
}
