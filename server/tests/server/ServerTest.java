/*
 * @author apmishutkin@edu.hse.ru
 */

package server;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class ServerTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    /**
     * Redirects all system IO streams.
     */
    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    /**
     * Redirects all system IO streams back.
     */
    @AfterEach
    public void restoreStreams() {
        Server.off();
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    /**
     * Starts and then shuts server. Can be used to test work with console args.
     *
     * @param args server console args
     */
    public static void runServerFor10ms(String[] args) {
        Thread t = new Thread(() -> Server.main(args));
        t.start();
        try {
            Thread.sleep(10);
        } catch (InterruptedException ignored) {
        }
        Server.off();
        try {
            t.join();
        } catch (InterruptedException ignored) {
        }
    }

    /**
     * Tests work with console args. For testing socket use run ClientThread's tests.
     * <p>
     * More info about flags after calling server with <code>--help</code> option.
     */
    @Test
    void main() {
        runServerFor10ms(new String[]{"kek"});
        assertTrue(errContent.size() > 0);
        errContent.reset();

        runServerFor10ms(new String[]{"-P=kek"});
        assertTrue(errContent.size() > 0);
        errContent.reset();

        runServerFor10ms(new String[]{"-P=10000000"});
        assertTrue(errContent.size() > 0);
        errContent.reset();

        runServerFor10ms(new String[]{"-H=barF312foo"});
        assertTrue(errContent.size() > 0);
        errContent.reset();

        runServerFor10ms(new String[]{"-D=...?"});
        assertTrue(errContent.size() > 0);
        errContent.reset();

        runServerFor10ms(new String[]{"-D=.", "--host=0.0.0.0", "-P=8080"});
        assertEquals(0, errContent.size());
    }
}