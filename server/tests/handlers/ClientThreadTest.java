/*
 * @author apmishutkin@edu.hse.ru
 */

package handlers;

import model.FileInfo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import server.Server;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Use stub client to test correctness of file giving process.
 */
class ClientThreadTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream fileStub = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    /**
     * Redirects all system IO streams.
     */
    @BeforeEach
    public void setUpStreams() {
        try (FileWriter fileWriter = new FileWriter("a.txt")) {
            fileWriter.write("SUCCESS");
        } catch (IOException exception) {
            fail("Test could not be passed due to file system issues...");
        }

        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    /**
     * Redirects all system IO streams back.
     */
    @AfterEach
    public void restoreStreams() {
        Server.off();
        System.setOut(originalOut);
        System.setErr(originalErr);
        boolean delete = new File("a.txt").delete();
        if (!delete) {
            System.out.println("Осторожно! Я не удалил файл за собой...");
        }
    }

    /**
     * Starts server, connects to it with mock client and then checks achieved file.
     * <p>
     * A kind of end to end test.
     * <p>
     * Timeout for socket operation is 500ms - enough for on-the-same-host use.
     */
    @Test
    void download() throws IOException, ClassNotFoundException {
        Thread t = new Thread(() -> Server.main(new String[]{"-P=8080", "-H=0.0.0.0", "-D=."}));
        t.start();
        int cnt = 0;
        while (!Server.serverOn.get() && cnt < 500) {
            cnt++;
            try {
                //noinspection BusyWait
                Thread.sleep(1);
            } catch (InterruptedException ignored) {

            }
        }

        Socket socket = new Socket("127.0.0.1", 8080);
        ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
        ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());

        assertTrue(in.readObject() instanceof ArrayList);
        FileInfo fileInfo = new FileInfo();
        fileInfo.setName("a.txt");
        out.writeObject(fileInfo);
        int size;
        while ((size = (int) in.readObject()) > 0) {
            fileStub.write((byte[]) in.readObject(), 0, size);
        }
        fileStub.flush();

        assertEquals("SUCCESS", fileStub.toString());
        socket.close();
        Server.off();

        try {
            t.join();
        } catch (InterruptedException ignored) {
        }
    }
}