/*
 * @author apmishutkin@edu.hse.ru
 */

package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Fully tests this simple class.
 */
class FileInfoTest {
    /**
     * Tests all getters and setters.
     */
    @Test
    void fileInfoTest() {
        FileInfo fileInfo = new FileInfo();
        fileInfo.setSize(2048);
        fileInfo.setName("kek");
        fileInfo.setPathToSave("kek/kek");

        assertEquals(String.format("%.2f КБ", 2.0), fileInfo.getSizeFormatted());
        assertEquals(2048, fileInfo.getSize());
        fileInfo.setSize(fileInfo.getSize() * 1024);
        assertEquals(String.format("%.2f МБ", 2.0), fileInfo.getSizeFormatted());
        fileInfo.setSize(fileInfo.getSize() * 1024);
        assertEquals(String.format("%.2f ГБ", 2.0), fileInfo.getSizeFormatted());
        fileInfo.setSize(0);
        assertEquals("0", fileInfo.getSizeFormatted());
        fileInfo.setSize(2);
        assertEquals(String.format("%d Б", 2), fileInfo.getSizeFormatted());
        fileInfo.setSize(2048);

        assertEquals("kek", fileInfo.getName());
        assertEquals("kek/kek", fileInfo.getPathToSave());

        assertEquals(2048, fileInfo.clone().getSize());
        assertEquals("kek", fileInfo.clone().getName());
        assertEquals("kek/kek", fileInfo.clone().getPathToSave());
        assertNotEquals(fileInfo, fileInfo.clone());
    }
}